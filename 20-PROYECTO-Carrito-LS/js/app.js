const carrito = document.querySelector("#carrito");
const contenedorCarrito = document.querySelector("#lista-carrito tbody");
const vaciarCarritoBtn = document.querySelector("#vaciar-carrito");
const listaCursos = document.querySelector("#lista-cursos");

let articulosCarrito = [];

cargarEventListeners();
function cargarEventListeners(){
    // Cuando agregas un curso presionando "Agregar al Carrito"
    listaCursos.addEventListener("click", agregarCurso);

    // Elimina cursos del carrito
    carrito.addEventListener("click", eliminarCurso);

    // Muestra los cursos de LocalStorage
    document.addEventListener("DOMContentLoaded", () => {
        articulosCarrito = JSON.parce(localStorage.getItem("carrito")) || [];
        carritoHTML();
    });

    // Vaciar el carrito
    vaciarCarritoBtn.addEventListener("click", () => {
        console.log("vaciando carrito . . .");
        articulosCarrito = []; // Reseteamos el arreglo
        limpiarHTML(); // Eliminamos todo el HTML
    });
}


//FUNCIONES
function agregarCurso(e){
    e.preventDefault();
    if(e.target.classList.contains("agregar-carrito")){
        // console.log(e.target.previousElementSibling );
        const cursoSeleccionado = e.target.parentElement.parentElement;
        // console.log("Agregando al carrito...");

        leerDatosCurso(cursoSeleccionado);

    }
};

// Elimina curso del carrito
function eliminarCurso(e) {
    console.log("desde eliminar curso");
    if(e.target.classList.contains("borrar-curso")){
        //console.log(e.target.getAttribute("data-id"));
        const cursoId = e.target.getAttribute("data-id");

        // Elimina del arreglo de articulos carrito por el data-id
        const curso = articulosCarrito.filter( cursos => cursos.id !== cursoId);
        articulosCarrito = [...curso];
        //console.log(articulosCarrito);
        
        // Iterar sobre el carrito y mostrar su HTML
        carritoHTML();
    }
};


// Lee el contenido del HTML al que le dimos click y extrae la informacion del curso
function leerDatosCurso(curso){
    console.log(curso);

    // Crear un objeto con el contenido del curso actual
    const infoCurso = {
        // imagen1: curso.children[0].src, una manera de hacerlo con traversing
        imagen: curso.querySelector("img").src,
        titulo: curso.querySelector("h4").textContent,
        precio: curso.querySelector(".precio span").textContent,
        id: curso.querySelector("a").getAttribute("data-id"),
        cantidad: 1
    };
    //console.log(infoCurso);

    // Revisa si un elemento ya existe en el carrito
    const existe = articulosCarrito.some( curso => curso.id === infoCurso.id)
    //console.log(existe);

    if (existe){
        // Actualizamos la cantidad
        const cursos = articulosCarrito.map( curso => {
            if( curso.id === infoCurso.id){
                curso.cantidad++;
                return curso; // retorn el objeto actualizado
            } else {
                return curso; // retorna los objetos que no son dupicados
            }
        });
        articulosCarrito = [...cursos];

    } else {
        // Agregamos el curso al carrito
        // Agrega elementos al arreglo de carrito
        articulosCarrito = [...articulosCarrito, infoCurso];
        console.log(articulosCarrito);
    }


    carritoHTML();
};

// Muestra el carrito de compras en el HTML
function carritoHTML(){

    // Limpiar el HTML
    limpiarHTML();

    // Recorre el carrito y genera el HTML
    articulosCarrito.forEach( curso => {
        const row = document.createElement("tr");
        const {titulo, imagen, precio, cantidad, id} = curso;
        row.innerHTML = `
        <td> <img src="${imagen}" width = "100"> </td>
        <td> ${titulo} </td>
        <td> ${precio} </td>
        <td> ${cantidad} </td>
        <td> <a href= "#" class="borrar-curso" data-id="${id}"> X </a> </td>
        `;

        // Agrega el HTML del carrito en el tbody
        contenedorCarrito.appendChild(row);
    });

    // Agregar el carrito de compras al Storage
    sincronizarStorage();

};

function sincronizarStorage(){
    console.log("desde sincronizar...");
    localStorage.setItem("carrito", JSON.stringify(articulosCarrito))
}




// Elimina los cursos del tbody
function limpiarHTML(){

    // Forma lenta
    /* contenedorCarrito.innerHTML = ""; */

    //forma rapida
    while(contenedorCarrito.firstChild){
        contenedorCarrito.removeChild(contenedorCarrito.firstChild);
    }
}