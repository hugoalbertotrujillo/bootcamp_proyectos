// Variables
const formulario = document.querySelector("#formulario");
const listaTweets = document.querySelector("#lista-tweets");

let tweets = [];

// EventListeners
eventListeners();
function eventListeners(){
    // Cuando el usuario crea un nuevo tweet
    formulario.addEventListener("submit", agregarTweet);

    // Cuando el documento esta listo
    document.addEventListener("DOMContentLoaded", () => {
        // esto significa que si el arreglo de entrada esta vacio, retorna un NULL, pero  la funcion crearHTML() esta preguntando por un length a un arreglo, asi que se genera un error. Para solucionar esto se pone el operador OR. Entonces, si el arrelgo de entrada esta en null se pasara a un arreglo vacio
        tweets = JSON.parse(localStorage.getItem("tweets")) || []; 

        // if (tweets){
        //     console.log("desde el LS...", tweets);
        //     crearHTML();
        //     return;
        // } 
        // tweets = [] MI VERSION
        
        console.log("desde el LSo...", tweets);
        crearHTML();
    });
}

// Funciones
function agregarTweet(e){
    e.preventDefault();

    // Textarea donde el usuario escribe
    const tweet = document.querySelector("#tweet").value;

    if(tweet === ""){
        mostrarError("No puede ir vacio");
        return;
    }

    const tweetObj = {
        id: Date.now(),
        tweet // Se puede poner tweet: tweet, pero eso se puede acortar como en este caso
    }

    // Añadir al arreglo de tweets
    tweets = [...tweets, tweetObj];
    console.log(tweets);

    // Crear el HTML
    crearHTML();

    // Reiniciar el formulario
    formulario.reset();

};

function mostrarError(mensaje){
    const contenido = document.querySelector("#contenido");
    
    const error = contenido.querySelector(".error");
    if(error){
        console.log("el arror");
        return;
    }

    const mensajeError = document.createElement("p");
    mensajeError.textContent = mensaje;
    mensajeError.classList.add("error");

    // Insertarlo en el contenido
    contenido.appendChild(mensajeError);

    // Elimiar la alerta
    setTimeout(() => {
        mensajeError.remove();
    }, 3000);
};

// Muestra un listado de los tweets
function crearHTML(){

    limpiarHTML();

    if(tweets.length > 0){
        tweets.forEach( tweet => {
            // Agregar boton de eliminar
            const btnEliminar = document.createElement("a");
            btnEliminar.classList.add("borrar-tweet");
            btnEliminar.textContent = "X";

            // Crear HTML
            const li = document.createElement("li");
            li.textContent = tweet.tweet;
            li.appendChild(btnEliminar);

            // Añadir la funcion de eliminar
            // eliminarTweet(li); MI VERSION
            btnEliminar.onclick = () => {
                borrarTweet(tweet.id);
            }

            // Insertarlo en HTML
            listaTweets.appendChild(li);
        });
    }

    // Agregar al storage
    sincronizarStorage();
};

// Agregar los tweets actuales al local storage
function sincronizarStorage(){
    localStorage.setItem("tweets", JSON.stringify(tweets));
}

// Limpiar el HTML
function limpiarHTML(){
    while(listaTweets.firstChild){
        listaTweets.removeChild(listaTweets.firstChild);
    }
};

// function eliminarTweet(referencia){
//     referencia.addEventListener("click", e => {
//         if(e.target.classList.contains("borrar-tweet")){
//             referencia.remove();
//         }
//     }); 
// } MI VERSION

function borrarTweet(id){
    console.log(id);
    tweets = tweets.filter( tweet => tweet.id !== id);
    tweets = [...tweets]
    console.log(tweets);

    crearHTML();

}

