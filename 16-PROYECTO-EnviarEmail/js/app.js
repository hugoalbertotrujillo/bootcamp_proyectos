// Asi nos aseguramos que ya se haya descargado todo el codigo HTML
document.addEventListener("DOMContentLoaded", function() {

    const email = {
        email: "",
        asunto: "",
        mensaje: ""
    };

    // Seleccionar los elementos de la interfaz
    const inputEmail = document.querySelector("#email");
    const inputAsunto = document.querySelector("#asunto");
    const inputMensaje = document.querySelector("#mensaje");
    const formulario = document.querySelector("#formulario");
    const btnSubmit = document.querySelector("#formulario button[type='submit']");
    const btnReset = document.querySelector("#formulario button[type='reset']");
    const spinner = document.querySelector("#spine");

    // Asignar Eventos
    inputEmail.addEventListener("input", validar);
    inputAsunto.addEventListener("input", validar);
    inputMensaje.addEventListener("input", validar);

    formulario.addEventListener("submit", enviarMail);

    btnReset.addEventListener("click", function(e) {
        e.preventDefault();

        resetFormulario()
    });

    // Funciones
    function validar(e) {

        // Con el trim() si el usuario pone solo espacios en blanco no pasara la validacion
        if(e.target.value.trim() === ""){
            mostrarAlerta(`El Campo ${e.target.id} es Obligatorio`, e.target.parentElement);
            email[e.target.name] = ""; // Se reinicia el campo que esta vacio
            comprobarEmail();
            return;
        }

       if(!validarEmail(e.target.value) && e.target.id === "email"){
                console.log("entro a validarEmail");
                mostrarAlerta("el Email no es valido", e.target.parentElement);
                email[e.target.name] = ""; // Se reinicia el campo que esta vacio
                comprobarEmail();
                return;
        }
        

        limpiarAlerta(e.target.parentElement);

        // Asignar los valores
        email[e.target.name] = e.target.value.trim().toLowerCase();
        
        // Comprobar el objeto de email
        comprobarEmail();
    };

    function mostrarAlerta(mensaje, referencia){
        
        // Comprueba si ya existe una alerta
        limpiarAlerta(referencia);

        // Generar alerta en HTML
        const error = document.createElement("p");
        error.textContent = mensaje;
        error.classList.add("bg-red-600", "text-white", "p-2", "text-center", "m-2", "alerta");
        // console.log(error);

        // Inyectar el error al formulario
        //formulario.innerHTML = error.innerHTML; // ESTO ES PARA AGREGAR ESTE MENSAJE Y QUE SE REEMPLACE TODO EL RESTO
        referencia.appendChild(error);
    };

    function limpiarAlerta(referencia){
        // console.log(referencia);
        // referencia.removeChild(referencia.lastChild); mi version

        const alerta = referencia.querySelector(".alerta");
        
        if(alerta){
            alerta.remove();
        }
    };

    function validarEmail(email){
        const regex =  /^\w+([.-_+]?\w+)*@\w+([.-]?\w+)*(\.\w{2,10})+$/;
        const resultado = regex.test(email);
        console.log(resultado);
        return resultado;
    }

    function comprobarEmail() {
        console.log(email);
        // Para verficar que en el array hay strings vacios se usa el .includes(), si da false es que ya no hay vacios
        if(Object.values(email).includes("")){
            btnSubmit.classList.add("opacity-50");
            btnSubmit.disabled = true;
        } else {
            btnSubmit.classList.remove("opacity-50");
            btnSubmit.disabled = false;
        };
    }

    function enviarMail(e){
        e.preventDefault();
        spinner.classList.add("flex");
        spinner.classList.remove("hidden");

        setTimeout(() => {
            spinner.classList.remove("flex");
            spinner.classList.add("hidden");

            resetFormulario();

            //Crear una alerta
            const alertaExito = document.createElement("p");
            alertaExito.classList.add("bg-green-500", "text-white", "p-2", "text-center", "rounded-lg", "mt-10", "font-bold", "text-sm", "uppercase");
            alertaExito.textContent = "mensaje enviado correctamente";
            formulario.appendChild(alertaExito);

            setTimeout(() => {
                alertaExito.remove();
            }, 3000);
            

        }, 5000);
    };

    function resetFormulario(){
        //reiniciar el objeto
        email.email = "";
        email.asunto = "";
        email.mensaje = "";

        formulario.reset();
        comprobarEmail();
    };

});