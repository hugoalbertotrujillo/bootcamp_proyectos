// Variables
const year = document.querySelector("#year");
const marca = document.querySelector("#marca");
const minimo = document.querySelector("#minimo");
const maximo = document.querySelector("#maximo");
const puertas = document.querySelector("#puertas");
const transmision = document.querySelector("#transmision");
const color = document.querySelector("#color");

// Contenedor para los resultados
const resultado = document.querySelector("#resultado");

const max = new Date().getFullYear();
const min = max - 10;

// generar un objeto con la busqueda
const datosBusqueda = {
    marca: "",
    year: "",
    minimo: "",
    maximo: "",
    puertas: "",
    transmision: "",
    color: ""
};


// Eventos
document.addEventListener("DOMContentLoaded", () => {

    mostrarAutos(autos); // muestra los autos al cargar
    llenarSelect(); // Llena las opciones de años

});

// Event Listener para los select de busqueda
marca.addEventListener("change", (e) => {
    datosBusqueda.marca = e.target.value
    console.log(datosBusqueda);

    filtrarAuto();
});

year.addEventListener("change", (e) => {
    datosBusqueda.year = e.target.value
    console.log(datosBusqueda);

    filtrarAuto();
});

minimo.addEventListener("change", (e) => {
    datosBusqueda.minimo = e.target.value
    console.log(datosBusqueda);

    filtrarAuto();
});

maximo.addEventListener("change", (e) => {
    datosBusqueda.maximo = e.target.value
    console.log(datosBusqueda);

    filtrarAuto();
});

puertas.addEventListener("change", (e) => {
    datosBusqueda.puertas = e.target.value
    console.log(datosBusqueda);

    filtrarAuto();
});

transmision.addEventListener("change", (e) => {
    datosBusqueda.transmision = e.target.value
    console.log(datosBusqueda);
    
    filtrarAuto();
});

color.addEventListener("change", (e) => {
    datosBusqueda.color = e.target.value
    console.log(datosBusqueda);
    
    filtrarAuto();
});



// Funciones
function mostrarAutos(autos) {
    
    limpiarHTML(); // Elimina el HTML previo

    autos.forEach( auto => {
        const {marca, modelo, year, puertas, transmision, precio, color} = auto;
        const autoHTML = document.createElement("p");

        autoHTML.textContent = `
            ${marca} ${modelo} - ${year} - ${puertas} Puertas - Trasnmision: ${transmision} - Precio: $${precio} - Color: ${color}
        `;

        // Insertar en el HTML
        resultado.appendChild(autoHTML);
    });
};

function limpiarHTML(){
    while(resultado.firstChild){
        resultado.removeChild(resultado.firstChild);
    }
};

function llenarSelect() {
    for(let i = max; i >= min; i--){
        const option = document.createElement("option");
        option.value = i;
        option.textContent = i;
        year.appendChild(option);
    };
};

// Funcion que filtra con base en la busqueda
function filtrarAuto() {
    console.log("filtrando...");

    const resultado = autos.filter( filtrarMarca ).filter( filtrarYear ).filter( filtrarMinimo ).filter( filtrarMaximo ).filter(filtrarPuertas).filter( filtrarTransmision ).filter( filtrarColor )
    // console.log(resultado);

    if(resultado.length){
        mostrarAutos(resultado);
    } else {
        noResultado();
    }
}

function filtrarMarca(auto){
    const { marca } = datosBusqueda;
    if (marca){
        return auto.marca === marca;
    }
    return auto;
};

function filtrarYear(auto){
    const { year } = datosBusqueda;
    if(year){
        return auto.year === parseInt(year);
    }
    return auto;
}

function filtrarMinimo(auto){
    const { minimo } = datosBusqueda;
    if(minimo){
        return auto.precio >= parseInt(minimo);
    }
    return auto;
};

function filtrarMaximo(auto){
    const { maximo } = datosBusqueda;
    if(maximo){
        return auto.precio <= parseInt(maximo);
    }
    return auto;
}

function filtrarPuertas(auto){
    const { puertas } = datosBusqueda;
    if(puertas){
        return auto.puertas === parseInt(puertas);
    }
    return auto;
};

function filtrarTransmision(auto){
    const { transmision } = datosBusqueda;
    if (transmision){
        return auto.transmision === transmision;
    }
    return auto;
};

function filtrarColor(auto){
    const { color } = datosBusqueda;
    if (color){
        return auto.color === color;
    }
    return auto;
};

function noResultado(){
    const noResultado = document.createElement("p");
    noResultado.textContent = " No Hay Coincidencias, intenta con otros terminos de busqueda"
    noResultado.classList.add("alerta", "error");

    limpiarHTML();
    resultado.appendChild(noResultado);
}